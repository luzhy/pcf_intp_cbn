(* main.ml *)

open Syntax ;;
open Eval ;;

let rec repl env chn = 
  print_string "> ";
  flush stdout;
  try
  let decl = Iparse.main Lex.token (Lexing.from_channel chn) in
  let res  = eval decl env in
    Printf.printf "- : ";
    Util.pprint_value res;
    print_newline();
    repl env chn
  with 
    | Lex.Lexer_Error s    -> 
        begin
          print_endline ("Error: " ^ s);
          repl env stdin
        end
    | Eval.Eval_Error s    -> 
        begin
          print_endline ("Error: " ^ s);
          repl env stdin
        end
    | Syntax.Parser_Error s -> 
        begin
          print_endline ("Error: " ^ s);
          repl env stdin
        end
    | Syntax.Parser_EOF -> print_endline "Exit"
;;

let _ = 
  begin
    print_endline "PCF Interpreter (Call by Name)";
    repl [] stdin
  end
;;
