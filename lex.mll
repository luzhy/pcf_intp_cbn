{
open Iparse
exception Lexer_Error of string
}


let space = [' ' '\t' '\r'] 
let retur = '\n'
let digit = ['0'-'9']
let alpha = ['A'-'Z' 'a'-'z' '_']
let alnum = digit | alpha | '\''

rule token = parse
| digit+    
  {
    let str = Lexing.lexeme lexbuf in
      INT (int_of_string str) 
  }

| '+'       { PLUS }
| '-'       { MINUS }
| '*'       { ASTERISK }
| '/'       { SLASH }
| '='       { EQUAL }

| '('       { LPAREN }
| ')'       { RPAREN }

| "->"      { ARROW }
| ";;"      { SEMISEMI }

| "fun"     { FUN }
| "let"     { LET }
| "in"      { IN }
| "ifz"     { IFZ }
| "then"    { THEN }
| "else"    { ELSE }
| "fix"     { FIX }

| alpha alnum*
  { VAR (Lexing.lexeme lexbuf) }

| eof       { EOF }

| space+    { token lexbuf }
           
| retur     { Printf.printf "  "; flush stdout; token lexbuf }

| _
            {
              let message = Printf.sprintf
                              "Unknown token %s near characters %d-%d"
                              (Lexing.lexeme lexbuf)
                              (Lexing.lexeme_start lexbuf)
                              (Lexing.lexeme_end lexbuf)
              in
                raise (Lexer_Error message)
            }
