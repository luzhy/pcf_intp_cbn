open Syntax 
;;

exception Eval_Error of string
;;

let ext env x v = (x,v) :: env ;;

let rec lookup x env =
  match env with
    | [] -> raise (Eval_Error ("Unbounded variable: " ^ x))
    | (y,v)::tl -> 
        if x = y then v 
        else lookup x tl 
;;

let apply_op op_type p q=
  match op_type, p, q with
    | Add, Const_Val(n1), Const_Val(n2) -> Const_Val(n1+n2)
    | Sub, Const_Val(n1), Const_Val(n2) -> Const_Val(n1-n2)
    | Mul, Const_Val(n1), Const_Val(n2) -> Const_Val(n1*n2)
    | Div, Const_Val(n1), Const_Val(n2) -> Const_Val(n1/n2)
    |   _, _, _ -> raise (Eval_Error "Binary operation error")
;;

let rec eval exp env = 
  match exp with
    | Var(x)       -> 
        let res = lookup x env in
        let t   = fst res in
        let e1  = snd res in
          eval t e1 

    | Bin_Op(op_type, t, u) -> 
        let q = eval u env in
        let p = eval t env in
          apply_op op_type p q

    | Const(n)     -> Const_Val(n)

    | If_Zero(t, u, v) ->
        begin
          match (eval t env) with
            | Const_Val(0) -> eval u env
            | Const_Val(n) -> eval v env
            | _ -> raise (Eval_Error "Ifz argument should be Const")
        end

    | Fix(x, t) ->
        let e1 = ext env x (Fix(x,t), env) in
          eval t e1

    | Func(x, t) -> Closure(x, t, env)

    | Let_In(x, t, u) ->
        let e1 = ext env x (t,env) in
          eval u e1

    | App(t,u)   -> 
        match eval t env with
          | Closure(x, t1, e1) -> 
              let e2 = ext e1 x (u,env) in
                eval t1 e2
          | _ -> raise (Eval_Error "Application error")
;;

