
exception Parser_Error of string
;;

exception Parser_EOF
;;

type binop = Add | Sub | Mul | Div
;;

type term =
    | Var     of string
    | Bin_Op  of binop  * term * term
    | Const   of int
    | If_Zero of term   * term * term
    | Fix     of string * term
    | Func    of string * term
    | Let_In  of string * term * term 
    | App     of term   * term
;;

type envir = (string * (term * envir)) list
;;

type value =
    | Const_Val of int
    | Closure  of string * term * envir
;;
